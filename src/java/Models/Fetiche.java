/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Giugliano
 */
public class Fetiche {
    
    private int ID;
    private String Descricao;
    private double Valor;
    private int ID_servicos;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public double getValor() {
        return Valor;
    }

    public void setValor(double Valor) {
        this.Valor = Valor;
    }

    public int getID_servicos() {
        return ID_servicos;
    }

    public void setID_servicos(int ID_servicos) {
        this.ID_servicos = ID_servicos;
    }
    
    
}

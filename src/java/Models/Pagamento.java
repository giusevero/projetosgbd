/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Date;

/**
 *
 * @author giusevero
 */
public class Pagamento {
    
    private int ID;
    private Date Data_Pagamento;
    private String Email;
    private int ID_pagamento;
    private int Parcelas;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getData_Pagamento() {
        return Data_Pagamento;
    }

    public void setData_Pagamento(Date Data_Pagamento) {
        this.Data_Pagamento = Data_Pagamento;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public int getID_pagamento() {
        return ID_pagamento;
    }

    public void setID_pagamento(int ID_pagamento) {
        this.ID_pagamento = ID_pagamento;
    }

    public int getParcelas() {
        return Parcelas;
    }

    public void setParcelas(int Parcelas) {
        this.Parcelas = Parcelas;
    }
    
    
    
}

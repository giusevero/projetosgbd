/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author giusevero
 */
public class Preferencias {
    
    private int ID;
    private String Descricao;
    private String Email_cliente;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public String getEmail_cliente() {
        return Email_cliente;
    }

    public void setEmail_cliente(String Email_cliente) {
        this.Email_cliente = Email_cliente;
    }
    
    
}

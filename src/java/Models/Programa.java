/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Date;

/**
 *
 * @author Giugliano
 */
public class Programa {
    
    private int ID;
    private String Email_cliente;
    private String Email_prestador;
    private int ID_servicos;
    private int ID_materiais;
    private Date Data;
    private double Valor;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getEmail_cliente() {
        return Email_cliente;
    }

    public void setEmail_cliente(String Email_cliente) {
        this.Email_cliente = Email_cliente;
    }

    public String getEmail_prestador() {
        return Email_prestador;
    }

    public void setEmail_prestador(String Email_prestador) {
        this.Email_prestador = Email_prestador;
    }

    public int getID_servicos() {
        return ID_servicos;
    }

    public void setID_servicos(int ID_servicos) {
        this.ID_servicos = ID_servicos;
    }

    public int getID_materiais() {
        return ID_materiais;
    }

    public void setID_materiais(int ID_materiais) {
        this.ID_materiais = ID_materiais;
    }

    public Date getData() {
        return Data;
    }

    public void setData(Date Data) {
        this.Data = Data;
    }

    public double getValor() {
        return Valor;
    }

    public void setValor(double Valor) {
        this.Valor = Valor;
    }
    
    
}

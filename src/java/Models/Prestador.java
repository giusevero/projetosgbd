/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author giusevero
 */
public class Prestador {
    
    private String Email;
    private String Nome_artistico;
    private int ID;
    private String Username;
    private String Genero;
    private boolean Fetiche;
    private double Latitude;
    private double Longitude;
    private int Telefone;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getNome_artistico() {
        return Nome_artistico;
    }

    public void setNome_artistico(String Nome_artistico) {
        this.Nome_artistico = Nome_artistico;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    public boolean isFetiche() {
        return Fetiche;
    }

    public void setFetiche(boolean Fetiche) {
        this.Fetiche = Fetiche;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double Latitude) {
        this.Latitude = Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double Longitude) {
        this.Longitude = Longitude;
    }

    public int getTelefone() {
        return Telefone;
    }

    public void setTelefone(int Telefone) {
        this.Telefone = Telefone;
    }
    
    
}

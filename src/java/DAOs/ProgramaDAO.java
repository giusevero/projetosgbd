
package DAOs;

import BD.Conexao;
import Models.Programa;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author luanna.silva
 */
public class ProgramaDAO {
    Conexao conexao;
     
     
    /**
     * Insere os valores no banco de dados
     * @param pagamento Objeto programa a ser inserido
     * @throws Exception 
     */
    public void InserePrograma (Programa programa) throws Exception{
        String SQL = "INSERT INTO programa (Valor, Cliente_Email, Prestador_Email, Servicos_ID, Materiais_ID)"
                + "VALUES (?, ?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            
            statement.setDouble(1, programa.getValor());
            statement.setString(2, programa.getEmail_cliente());
            statement.setString(3, programa.getEmail_prestador());
            statement.setInt(4, programa.getID_servicos());
            statement.setInt(5, programa.getID_materiais());
            
            statement.execute();
            
        } catch (Exception e) {
            throw new Exception("Falha ao inserir o programa: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os programas do banco
     * @return Lista com data
     */
    public List<Programa> SelecionaPrograma() throws Exception{
        List <Programa>listaPrograma = null;
        Statement statement = null;
        Programa programa = null;
        
        String SQL = "SELECT Data FROM programa";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                programa = new Programa();
                programa.setData(rs.getDate("Data"));
                listaPrograma.add(programa);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar os programas: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaPrograma;
    }
    
  
    public void atualizaPrograma(Programa programa) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE programa SET Data = ? "
                + "WHERE ID = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setDate(1, programa.getData());
            statement.setInt(2, programa.getID());
           
            
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
  
    public Programa selecionaPrograma(int ID) throws Exception{
        Programa programa  = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM programa WHERE ID = "+ID;
        
           try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            programa.setID(rs.getInt("ID"));
            programa.setData(rs.getDate("Data"));
            
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar um programa: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return programa;
    }
    
    /**
     * Deleta um programa do banco 
     * @param progama Objeto a ser deletado
     */
    public void deletaPrograma(Programa programa) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE programa WHERE ID = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, programa.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}
    



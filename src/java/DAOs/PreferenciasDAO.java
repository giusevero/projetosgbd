
package DAOs;

import BD.Conexao;
import Models.Preferencias;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author luanna.silva
 */
public class PreferenciasDAO {
    Conexao conexao;
     
     
    /**
     * Insere os valores no banco de dados
     * @param preferencias Objeto preferencias a ser inserido
     * @throws Exception 
     */
    public void InserePreferencias (Preferencias preferencias) throws Exception{
        String SQL = "INSERT INTO preferencias (Descricao)"
                + "VALUES (?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, preferencias.getDescricao());
            
        } catch (Exception e) {
            throw new Exception("Falha ao inserir a preferencia: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos as preferencias do banco
     * @return Lista com descrição
     */
    public List<Preferencias> SelecionaPreferencias() throws Exception{
        List <Preferencias>listaPreferencias = null;
        Statement statement = null;
        Preferencias preferencias = null;
        
        String SQL = "SELECT Descricao FROM preferencias";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                preferencias = new Preferencias();
                preferencias.setDescricao(rs.getString("Descricao"));
                listaPreferencias.add(preferencias);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar as preferencias: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaPreferencias;
    }
    
  
    public void atualizaPreferencias(Preferencias preferencias) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE preferencias SET Descricao = ? "
                + "WHERE ID = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, preferencias.getDescricao());
            statement.setInt(2, preferencias.getID());
           
            
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
  
    public Preferencias selecionaPreferencias(int ID) throws Exception{
        Preferencias preferencias  = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM preferencias WHERE ID = "+ID;
        
           try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            preferencias.setID(rs.getInt("ID"));
            preferencias.setDescricao(rs.getString("Descricao"));
            
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar uma preferencia: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return preferencias;
    }
    
    /**
     * Deleta uma preferencia do banco 
     * @param preferencias Objeto a ser deletado
     */
    public void deletaPreferencias(Preferencias preferencias) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE preferencias WHERE ID = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, preferencias.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}
    


  

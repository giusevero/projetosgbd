
package DAOs;
import BD.Conexao;
import Models.Fetiche;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author luanna.silva
 */
public class FeticheDAO {
        Conexao conexao;
    /**
     * Insere os valores no banco de dados
     * @param fetiche Objeto fetiche a ser inserido
     * @throws Exception 
     */
    public void InsereFetiche (Fetiche fetiche) throws Exception{
        String SQL = "INSERT INTO fetiche (Descricao, Valor)"
                + "VALUES (?, ?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, fetiche.getDescricao());
            statement.setDouble(2, fetiche.getValor());
        } catch (Exception e) {
            throw new Exception("Falha ao inserir o fetiche: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os fetiche do banco
     * @return Lista com descrição
     */
    public List<Fetiche> SelecionaFetiche() throws Exception{
        List <Fetiche>listaFetiche = null;
        Statement statement = null;
        Fetiche fetiche = null;
        
        String SQL = "SELECT Descricao FROM fetiche";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                fetiche = new Fetiche();
                fetiche.setDescricao(rs.getString("Descrição"));
                listaFetiche.add(fetiche);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar os fetiches: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaFetiche;
    }
    
  
    public void atualizaFetiche(Fetiche fetiche) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE fetiche SET Descricao = ?, Valor = ? "
                + "WHERE ID = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, fetiche.getDescricao());
            statement.setDouble(2, fetiche.getValor());
            statement.setInt(3, fetiche.getID());
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Seleciona um fetiche do banco
     * @param ID Id numérico do fetiche
     * @return Objeto fetiche
     * @throws Exception 
     */
    public Fetiche selecionaUmFetiche(int ID) throws Exception{
        Fetiche fetiche  = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM fetiche WHERE ID = "+ID;
        
        try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            fetiche.setID(rs.getInt("ID"));
            fetiche.setDescricao(rs.getString("Descricao"));
            fetiche.setValor(rs.getDouble("Valor"));
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar um fetiche: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return fetiche;
    }
    
    /**
     * Deleta um fetiche do banco 
     * @param fetiche Objeto a ser deletado
     */
    public void deletaFetiche(Fetiche fetiche) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE fetiche WHERE ID = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, fetiche.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}


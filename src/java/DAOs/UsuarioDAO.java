/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import BD.Conexao;
import Models.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author Giugliano
 */
public class UsuarioDAO {
    Conexao conexao;
    
    /**
     * Insere os valores no banco de dados
     * @param usuario Objeto usuario a ser inserido
     * @throws Exception 
     */
    public void InsereUsuario (Usuario usuario) throws Exception{
        String SQL = "INSERT INTO usuario (Username, Senha, ID)"
                + "VALUES (?, ?, ?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, usuario.getUsername());
            statement.setString(2, usuario.getSenha());
            statement.setInt(3, usuario.getID());
            
            statement.execute();
        } catch (Exception e) {
            throw new Exception("Falha ao inserir usuário: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os usernames do banco
     * @return Lista com usernames
     */
    public List<Usuario> SelecionaUsuario() throws Exception{
        List <Usuario>listaUsuario = null;
        Statement statement = null;
        Usuario usuario = null;
        
        String SQL = "SELECT username FROM usuario";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                usuario = new Usuario();
                usuario.setUsername(rs.getString("sername"));
                listaUsuario.add(usuario);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao lista usuários: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaUsuario;
    }
    
    /**
     * Atualiza informações do usuário
     * @param usuario Objeto a ser atualizado
     * @throws Exception 
     */
    public void atualizaUsuario(Usuario usuario) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE usuario SET username = ?,senha = ? "
                + "WHERE id = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, usuario.getUsername());
            statement.setString(2, usuario.getSenha());
            statement.setInt(3, usuario.getID());
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar usuario: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Seleciona um usuario do banco
     * @param ID Id numérico do usuário
     * @return Objeto usuário
     * @throws Exception 
     */
    public Usuario selecionaUmUsuario(int ID) throws Exception{
        Usuario usuario = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM usuario WHERE id = "+ID;
        
        try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            usuario.setID(rs.getInt("id"));
            usuario.setUsername(rs.getString("username"));
            usuario.setSenha(rs.getString("senha"));
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar um usuário: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return usuario;
    }
    
    /**
     * Deleta um usuário do banco 
     * @param usuario Objeto a ser deletado
     */
    public void deletaUsuario(Usuario usuario) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE usuario WHERE id = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, usuario.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar usuario: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
    
    public int retornaIDMax() throws Exception{
        int retorno = 0;
        Statement statement = null;
        String SQL = "SELECT max(usuario.ID) AS ultimo FROM usuario";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            
            while(rs.next()){
                retorno = rs.getInt("ultimo");
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Falha ao buscar max : "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return retorno +1;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import BD.Conexao;
import Models.Servicos;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luanna.silva
 */
public class ServicosDAO {
    
    Conexao conexao;
    /**
     * Insere os valores no banco de dados
     * @param servicos Objeto servicos a ser inserido
     * @throws Exception 
     */
    public void InsereServicos (Servicos servicos) throws Exception{
        String SQL = "INSERT INTO servicos (Descricao, Valor)"
                + "VALUES (?, ?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, servicos.getDescricao());
            statement.setDouble(2, servicos.getValor());
        } catch (Exception e) {
            throw new Exception("Falha ao inserir: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os serviços do banco
     * @return Lista com descrição
     */
    public List<Servicos> SelecionaServicos() throws Exception{
        List <Servicos>listaServicos = new ArrayList<>();
        Statement statement = null;
        Servicos servicos = null;
        
        String SQL = "SELECT * FROM servicos";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                servicos = new Servicos();
                servicos.setID(rs.getInt("ID"));
                servicos.setDescricao(rs.getString("Descricao"));
                servicos.setValor(rs.getDouble("Valor"));
                listaServicos.add(servicos);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar os serviços: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaServicos;
    }
    
  
    public void atualizaServicos(Servicos servicos) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE servicos SET Descricao = ?, Valor = ? "
                + "WHERE ID = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, servicos.getDescricao());
            statement.setDouble(2, servicos.getValor());
            statement.setInt(3, servicos.getID());
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Seleciona um serviço do banco
     * @param ID Id numérico do serviço
     * @return Objeto servicos
     * @throws Exception 
     */
    public Servicos selecionaUmServico(int ID) throws Exception{
        Servicos servicos = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM servico WHERE ID = "+ID;
        
        try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            servicos.setID(rs.getInt("ID"));
            servicos.setDescricao(rs.getString("Descricao"));
            servicos.setValor(rs.getDouble("Valor"));
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar um serviço: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return servicos;
    }
    
    /**
     * Deleta um serviço do banco 
     * @param servicos Objeto a ser deletado
     */
    public void deletaServico(Servicos servicos) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE servico WHERE ID = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, servicos.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar serviço: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}

    



package DAOs;

import BD.Conexao;
import Models.FormaPagamento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luanna.silva
 */
public class FormaPagamentoDAO {
    Conexao conexao;
     
     
    /**
     * Insere os valores no banco de dados
     * @param formaPagamento Objeto FormaPagamento a ser inserido
     * @throws Exception 
     */
    public void InsereFormaPagamento (FormaPagamento formaPagamento) throws Exception{
        String SQL = "INSERT INTO formaPagamento (Descricao)"
                + "VALUES (?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, formaPagamento.getDescricao());
            
        } catch (Exception e) {
            throw new Exception("Falha ao inserir a forma de pagamento: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos as formas de pagamento do banco
     * @return Lista com descrição
     */
    public List<FormaPagamento> SelecionaFormaPagamento() throws Exception{
        List <FormaPagamento>listaFormaPagamento = null;
        Statement statement = null;
        FormaPagamento formaPagamento = null;
        
        String SQL = "SELECT Descricao FROM formaPagamento";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                formaPagamento = new FormaPagamento();
                formaPagamento.setDescricao(rs.getString("Descricao"));
                listaFormaPagamento.add(formaPagamento);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar as formas de pagamento: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaFormaPagamento;
    }
    
  
    public void atualizaFormaPagamento(FormaPagamento formaPagamento) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE formaPagamento SET Descricao = ? "
                + "WHERE ID = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, formaPagamento.getDescricao());
            statement.setInt(2, formaPagamento.getID());
           
            
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
  
    public FormaPagamento selecionaUmaFormaPagamento(int ID) throws Exception{
        FormaPagamento formaPagamento  = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM formaPagamento WHERE ID = "+ID;
        
           try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            formaPagamento.setID(rs.getInt("ID"));
            formaPagamento.setDescricao(rs.getString("Descricao"));
            
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar uma forma de pagamento: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return formaPagamento;
    }
    
    /**
     * Deleta uma forma de pagamento do banco 
     * @param formaPagamento Objeto a ser deletado
     */
    public void deletaFormaPagamento(FormaPagamento formaPagamento) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE formaPagamento WHERE ID = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, formaPagamento.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}
    


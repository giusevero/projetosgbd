package DAOs;
import BD.Conexao;
import Models.Materiais;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luanna.silva
 */
public class MateriaisDAO {
     Conexao conexao;
    /**
     * Insere os valores no banco de dados
     * @param materiais Objeto materiais a ser inserido
     * @throws Exception 
     */
    public void InsereMateriais (Materiais materiais) throws Exception{
        String SQL = "INSERT INTO materiais (Descricao, Valor)"
                + "VALUES (?, ?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, materiais.getDescricao());
            statement.setDouble(2, materiais.getValor());
        } catch (Exception e) {
            throw new Exception("Falha ao inserir: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os materiais do banco
     * @return Lista com descrição
     */
    public List<Materiais> SelecionaMateriais() throws Exception{
        List <Materiais>listaMateriais = new ArrayList<>();
        Statement statement = null;
        Materiais materiais = null;
        
        String SQL = "SELECT * FROM materiais";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                materiais = new Materiais();
                materiais.setID(rs.getInt("ID"));
                materiais.setDescricao(rs.getString("Descricao"));
                materiais.setValor(rs.getDouble("Valor"));
                listaMateriais.add(materiais);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar os materiais: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaMateriais;
    }
    
  
    public void atualizaMateriais(Materiais materiais) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE materiais SET Descricao = ?, Valor = ? "
                + "WHERE ID = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, materiais.getDescricao());
            statement.setDouble(2, materiais.getValor());
            statement.setInt(3, materiais.getID());
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Seleciona um material do banco
     * @param ID Id numérico do material
     * @return Objeto materiais
     * @throws Exception 
     */
    public Materiais selecionaUmMaterial(int ID) throws Exception{
        Materiais materiais = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM materiais WHERE ID = "+ID;
        
        try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            materiais.setID(rs.getInt("ID"));
            materiais.setDescricao(rs.getString("Descricao"));
            materiais.setValor(rs.getDouble("Valor"));
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar um material: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return materiais;
    }
    
    /**
     * Deleta um material do banco 
     * @param materiais Objeto a ser deletado
     */
    public void deletaMaterial(Materiais materiais) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE materiais WHERE ID = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, materiais.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar o material: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}



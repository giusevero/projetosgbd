
package DAOs;

import BD.Conexao;
import Models.Pagamento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luanna.silva
 */
public class PagamentoDAO {
    Conexao conexao;
     
     
    /**
     * Insere os valores no banco de dados
     * @param pagamento Objeto pagamento a ser inserido
     * @throws Exception 
     */
    public void InserePagamento (Pagamento pagamento) throws Exception{
        String SQL = "INSERT INTO pagamento (Data_Pagamento, Parcelas)"
                + "VALUES (?, ?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setDate(1, pagamento.getData_Pagamento());
            statement.setInt(2, pagamento.getParcelas());
            
        } catch (Exception e) {
            throw new Exception("Falha ao inserir o pagamento: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os pagamentos do banco
     * @return Lista com data
     */
    public List<Pagamento> SelecionaPagamento() throws Exception{
        List <Pagamento>listaPagamento = null;
        Statement statement = null;
        Pagamento pagamento = null;
        
        String SQL = "SELECT Data_Pagamento FROM pagamento";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                pagamento = new Pagamento();
                pagamento.setData_Pagamento(rs.getDate("Data_Pagamento"));
                listaPagamento.add(pagamento);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar os pagamentos: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaPagamento;
    }
    
  
    public void atualizaPagamento(Pagamento pagamento) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE pagamento SET Data_Pagamento = ? "
                + "WHERE ID = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setDate(1, pagamento.getData_Pagamento());
            statement.setInt(2, pagamento.getID());
           
            
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
  
    public Pagamento selecionaPagamento(int ID) throws Exception{
        Pagamento pagamento  = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM pagamento WHERE ID = "+ID;
        
           try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            pagamento.setID(rs.getInt("ID"));
            pagamento.setData_Pagamento(rs.getDate("Data_Pagamento"));
            
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar um pagamento: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return pagamento;
    }
    
    /**
     * Deleta um pagamento do banco 
     * @param pagamento Objeto a ser deletado
     */
    public void deletaPagamento(Pagamento pagamento) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE pagamento WHERE ID = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, pagamento.getID());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}
    




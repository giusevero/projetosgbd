/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import BD.Conexao;
import Models.Cliente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Giugliano
 */
public class ClienteDAO {
    
    Conexao conexao;
    
    /**
     * Insere um cliente no banco de dados
     * @param cliente
     * @throws Exception 
     */
    public void InsereClientes(Cliente cliente) throws Exception{
        
        String SQL = "INSERT INTO cliente (Email, Fetiche, Nome, Genero, Usuario_ID, Usuario_Username, Telefone)"+
                " VALUES (?,?,?,?,?,?,?)";
        
        PreparedStatement statement =  null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, cliente.getEmail());
            statement.setBoolean(2, cliente.isFetiche());
            statement.setString(3, cliente.getNome());
            statement.setString(4, cliente.getGenero());
            statement.setInt(5, cliente.getID());
            statement.setString(6, cliente.getUsername());
            statement.setInt(7, cliente.getTelefone());
            
            statement.execute();
        } catch (Exception e) {
            throw new Exception("Falha ao inserir cliente: "+e.getMessage());
        } finally{
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Realiza busca no banco para os clientes cadastrados
     * @return Lista com os clientes 
     * @throws Exception 
     */
    public List<Cliente> ListaClientes() throws Exception{
        List<Cliente> retorno = new ArrayList<>();
        Cliente cliente = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM cliente order by Usuario_ID asc";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            
            while(rs.next()){
                cliente = new Cliente();
                cliente.setID(rs.getInt("Usuario_ID"));
                cliente.setEmail(rs.getString("Email"));
                cliente.setNome(rs.getString("Nome"));
                cliente.setUsername(rs.getString("Usuario_Username")); 
                cliente.setGenero(rs.getString("Genero"));
                cliente.setFetiche(rs.getBoolean("Fetiche"));
                cliente.setTelefone(rs.getInt("Telefone"));
                
                retorno.add(cliente);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar clientes: "+e.getMessage());
        } finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return retorno;
    }
}

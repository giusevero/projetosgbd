/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import BD.Conexao;
import Models.Prestador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Giugliano
 */
public class PrestadorDAO {
    
    Conexao conexao;
    
    public void InserePrestador(Prestador prestador) throws Exception{
        
        String SQL = "INSERT INTO prestador (Email, Genero, Fetiche, Usuario_ID, Usuario_Username, Telefone, Nome_artistico)"+
                "VALUES (?,?,?,?,?,?,?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setString(1, prestador.getEmail());
            statement.setString(2, prestador.getGenero());
            statement.setBoolean(3, prestador.isFetiche());
            statement.setInt(4, prestador.getID());
            statement.setString(5, prestador.getUsername());
            statement.setInt(6, prestador.getTelefone());
            statement.setString(7, prestador.getNome_artistico());
            
            statement.execute();
        } catch (Exception e) {
            throw new Exception("Falha ao inserir usuário: "+e.getMessage());
        } finally{
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os prestadores cadastrados
     * @return Retorna lista com todas os prestadores
     * @throws Exception 
     */
    public List<Prestador> ListaPrestador() throws Exception{
        List<Prestador> retorno = new ArrayList<>();
        Prestador prestador = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM prestador ORDER BY Usuario_ID ASC";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while(rs.next()){
                prestador = new Prestador();
                
                prestador.setID(rs.getInt("Usuario_ID"));
                prestador.setEmail(rs.getString("Email"));
                prestador.setNome_artistico(rs.getString("Nome_artistico"));
                prestador.setUsername(rs.getString("Usuario_Username"));
                prestador.setGenero(rs.getString("Genero"));
                prestador.setFetiche(rs.getBoolean("Fetiche"));
                prestador.setTelefone(rs.getInt("Telefone"));
                
                retorno.add(prestador);
            }
            
        } catch (Exception e) {
            throw new Exception("Falha ao listar prestador: "+e.getMessage());
        }
        
        return retorno;
    }
}

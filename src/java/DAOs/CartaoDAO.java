
package DAOs;

import BD.Conexao;
import Models.Cartao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luanna.silva
 */
public class CartaoDAO {
     Conexao conexao;
     
     
    /**
     * Insere os valores no banco de dados
     * @param cartao Objeto cartao a ser inserido
     * @throws Exception 
     */
    public void InsereCartao (Cartao cartao) throws Exception{
        String SQL = "INSERT INTO cartao (Numero, Codigo, Nome)"
                + "VALUES (?, ?, ?)";
        
        PreparedStatement statement = null;
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, cartao.getNumero());
            statement.setInt(2, cartao.getCodigo());
            statement.setString(3, cartao.getNome());
        } catch (Exception e) {
            throw new Exception("Falha ao inserir o cartao: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
    /**
     * Lista todos os cartões do banco
     * @return Lista com descrição
     */
    public List<Cartao> SelecionaCartao() throws Exception{
        List <Cartao>listaCartao = null;
        Statement statement = null;
        Cartao cartao = null;
        
        String SQL = "SELECT Numero FROM cartao";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().createStatement();
            ResultSet rs = statement.executeQuery(SQL);
            
            while (rs.next()) {
                cartao = new Cartao();
                cartao.setNumero(rs.getInt("Numero"));
                listaCartao.add(cartao);
            }
        } catch (Exception e) {
            throw new Exception("Falha ao listar os cartões: "+e.getMessage());
        } finally {
            statement.close();
            conexao.Conexao().close();
        }
        
        return listaCartao;
    }
    
  
    public void atualizaCartao(Cartao cartao) throws Exception{
        PreparedStatement statement = null;
        
        String SQL = "UPDATE cartao SET Numero = ?, Codigo = ?, Nome = ? "
                + "WHERE Numero = ?";
        
        try {
            conexao = new Conexao();
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, cartao.getNumero());
            statement.setInt(2, cartao.getCodigo());
            statement.setString(3, cartao.getNome());
            
            
            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao atualizar: "+e.getMessage());
        }finally {
            statement.close();
            conexao.Conexao().close();
        }
    }
    
  
    public Cartao selecionaUmCartao(int Numero) throws Exception{
        Cartao cartao  = null;
        Statement statement = null;
        
        String SQL = "SELECT * FROM cartao WHERE Numero = "+Numero;
        
        try {
            statement = conexao.Conexao().createStatement();
            
            ResultSet rs = statement.executeQuery(SQL);
            cartao.setNumero(rs.getInt("Nmero"));
            cartao.setCodigo(rs.getInt("Codigo"));
            cartao.setNome(rs.getString("Nome"));
            
        } catch (Exception e) {
            throw new Exception("Falha ao selecionar um cartao: "+e.getMessage());
        }finally{
            statement.close();
            conexao.Conexao().close();
        }
        
        return cartao;
    }
    
    /**
     * Deleta um cartao do banco 
     * @param cartao Objeto a ser deletado
     */
    public void deletaCartao(Cartao cartao) throws Exception{
        PreparedStatement statement = null;
        conexao = new Conexao();
        String SQL = "DELETE cartao WHERE Numero = ?";
        
        try {
            
            statement = conexao.Conexao().prepareStatement(SQL);
            
            statement.setInt(1, cartao.getNumero());

            statement.executeUpdate();
            
        } catch (Exception e) {
            throw new Exception("Falha ao deletar: "+e.getMessage());
        } finally {
            conexao.Conexao().close();
            statement.close();
        }
        
    }
}
    

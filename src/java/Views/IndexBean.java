/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import DAOs.ClienteDAO;
import DAOs.MateriaisDAO;
import DAOs.PrestadorDAO;
import DAOs.ProgramaDAO;
import DAOs.ServicosDAO;
import DAOs.UsuarioDAO;
import Models.Cliente;
import Models.Materiais;
import Models.Prestador;
import Models.Programa;
import Models.Servicos;
import Models.Usuario;
import Util.Strings;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.ws.rs.client.Client;

/**
 *
 * @author Giugliano
 */
@Named(value = "indexBean")
@ViewScoped
public class IndexBean implements Serializable{

    private ClienteDAO clienteDAO;
    private PrestadorDAO prestadorDAO;
    private UsuarioDAO usuarioDAO;
    private MateriaisDAO materiaisDAO;
    private ServicosDAO servicosDAO;
    private ProgramaDAO programaDAO;
    
    private List<Cliente> listaCLiente;
    private List<Prestador> listaPrestador;
    private List<Materiais> listaMateriais;
    private List<Servicos> listaServicos;
    
    private Prestador prestador;
    private Cliente cliente;
    private Usuario usuario;
    private Programa programa;
    
    private Cliente clienteSele;
    private Prestador prestadorSele;
    
    private Cliente clienteProg;
    private Prestador prestadorProg;
    private Materiais materiaisProg;
    private Servicos servicosProg;
    
    private Cliente cliente1;
    
    private String foneP;
    private String foneC;
    private List<String> generosP;
    private List<String> generosC;
    
    /**
     * Creates a new instance of IndexBean
     */
    public IndexBean() {
        clienteDAO = new ClienteDAO();
        prestadorDAO = new PrestadorDAO();
        usuarioDAO = new UsuarioDAO();
        servicosDAO = new ServicosDAO();
        materiaisDAO = new MateriaisDAO();
        programaDAO = new ProgramaDAO();
        
        prestador = new Prestador();
        cliente = new Cliente();
        usuario = new Usuario();
        
        clienteSele = new Cliente();
        prestadorSele = new Prestador();
    }
    
    /**
     * Método de reinicialização de variáveis
     */
    private void novoUsuario(){
        prestador = new Prestador();
        cliente = new Cliente();
        usuario = new Usuario();
    }
    
    /**
     * Lista os clientes cadastrados
     * @return Lista com os clientes
     */
    public List<Cliente>ListarCliente(){
        listaCLiente = new ArrayList<>();
        
        try {
            listaCLiente = clienteDAO.ListaClientes();
        } catch (Exception ex) {
            Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaCLiente;
    }
    
    /**
     * Lista os prestadores cadastrados
     * @return Lista os prestadores
     */
    public List<Prestador> ListarPrestador(){
        listaPrestador = new ArrayList<>();
        try {
            listaPrestador = prestadorDAO.ListaPrestador();
        } catch (Exception e) {
            Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaPrestador;
    }
    
    public List<Materiais> ListarMateriais(){
        listaMateriais = new ArrayList<>();
        
        try {
            listaMateriais = materiaisDAO.SelecionaMateriais();
        } catch (Exception e) {
            Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaMateriais;
    }

    public List<Servicos> ListarServicos(){
        listaServicos = new ArrayList<>();
        
        try {
            listaServicos = servicosDAO.SelecionaServicos();
        } catch (Exception e) {
            Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return listaServicos;
    }
    
    public void Salvar(){
     
        try {
//            Usuario user =  usuario;
//            Prestador prest = prestador;
//            Cliente c = cliente;
//            String i = foneC;
//            String f = foneP;
//            List<String> l = generosC;
//            List<String> z = generosP;
            int id = usuarioDAO.retornaIDMax();

            
            usuario.setID(id);
            usuarioDAO.InsereUsuario(usuario);
            if (!prestador.getEmail().equals("")) {
                prestador.setID(usuario.getID());
                prestador.setUsername(usuario.getUsername());
                //prestador.setTelefone(Strings.formataTelefone(foneP));
                prestador.setGenero(generosP.get(0));
                prestadorDAO.InserePrestador(prestador);
                
            } else if (!cliente.getEmail().equals("")) {
                cliente.setID(usuario.getID());
                cliente.setUsername(usuario.getUsername());
                //cliente.setTelefone(Strings.formataTelefone(foneC));
                cliente.setGenero(generosC.get(0));
                clienteDAO.InsereClientes(cliente);
                
            }

        } catch (Exception e) {
            Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, e);
        }
        
        novoUsuario();
    }
    
    public void SalvarPrograma(){
        //Usuario user =  usuario;
      Prestador prest = prestadorProg;
      
      Cliente c = clienteProg;
      Materiais m = materiaisProg;
      Servicos s = servicosProg;
      
        try {
            
            programa.setEmail_cliente(clienteProg.getEmail());
            programa.setEmail_prestador(prestadorProg.getEmail());
            programa.setID_materiais(materiaisProg.getID());
            programa.setID_servicos(servicosProg.getID());
            
            programaDAO.InserePrograma(programa);

        } catch (Exception ex) {
            Logger.getLogger(IndexBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**************************************************************************************************************************/
    
    public List<Cliente> getListaCLiente() {
        return listaCLiente;
    }

    public void setListaCLiente(List<Cliente> listaCLiente) {
        this.listaCLiente = listaCLiente;
    }

    public List<Prestador> getListaPrestador() {
        return listaPrestador;
    }

    public void setListaPrestador(List<Prestador> listaPrestador) {
        this.listaPrestador = listaPrestador;
    }

    public Prestador getPrestador() {
        return prestador;
    }

    public void setPrestador(Prestador prestador) {
        this.prestador = prestador;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getFoneP() {
        return foneP;
    }

    public void setFoneP(String foneP) {
        this.foneP = foneP;
    }

    public String getFoneC() {
        return foneC;
    }

    public void setFoneC(String foneC) {
        this.foneC = foneC;
    }

    public List<String> getGenerosP() {
        return generosP;
    }

    public void setGenerosP(List<String> generosP) {
        this.generosP = generosP;
    }

    public List<String> getGenerosC() {
        return generosC;
    }

    public void setGenerosC(List<String> generosC) {
        this.generosC = generosC;
    }

    public Cliente getClienteSele() {
        return clienteSele;
    }

    public void setClienteSele(Cliente clienteSele) {
        this.clienteSele = clienteSele;
    }

    public Prestador getPrestadorSele() {
        return prestadorSele;
    }

    public void setPrestadorSele(Prestador prestadorSele) {
        this.prestadorSele = prestadorSele;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public List<Materiais> getListaMateriais() {
        return listaMateriais;
    }

    public void setListaMateriais(List<Materiais> listaMateriais) {
        this.listaMateriais = listaMateriais;
    }

    public List<Servicos> getListaServicos() {
        return listaServicos;
    }

    public void setListaServicos(List<Servicos> listaServicos) {
        this.listaServicos = listaServicos;
    }

    public Cliente getClienteProg() {
        return clienteProg;
    }

    public void setClienteProg(Cliente clienteProg) {
        this.clienteProg = clienteProg;
    }

    public Prestador getPrestadorProg() {
        return prestadorProg;
    }

    public void setPrestadorProg(Prestador prestadorProg) {
        this.prestadorProg = prestadorProg;
    }

    public Materiais getMateriaisProg() {
        return materiaisProg;
    }

    public void setMateriaisProg(Materiais materiaisProg) {
        this.materiaisProg = materiaisProg;
    }

    public Servicos getServicosProg() {
        return servicosProg;
    }

    public void setServicosProg(Servicos servicosProg) {
        this.servicosProg = servicosProg;
    }

    
    
    
    
}

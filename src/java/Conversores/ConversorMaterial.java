/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conversores;

import DAOs.MateriaisDAO;
import Models.Materiais;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author giuse
 */

@FacesConverter("Material")
public class ConversorMaterial implements Converter{

    MateriaisDAO materiaisDAO;
    Materiais materiais = new Materiais();
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        materiaisDAO = new MateriaisDAO();
        
        try {
            List <Materiais> mat = materiaisDAO.SelecionaMateriais();
            
            for (Materiais materiais1 : mat) {
                if(value.equals(materiais1.getDescricao())){
                    materiais = materiais1;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ConversorMaterial.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return materiais;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value != null){
            return ((Materiais) value).getDescricao();
        } else{
            return null;
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conversores;

import DAOs.ClienteDAO;
import Models.Cliente;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author giuse
 */

@FacesConverter("Cliente")
public class ConversorCliente implements Converter{

    ClienteDAO clienteDAO;
    Cliente cliente = new Cliente();
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        clienteDAO = new ClienteDAO();
        
        try {
            List<Cliente> cli = clienteDAO.ListaClientes();
            
            for (Cliente cliente1 : cli) {
                if(value.equals(cliente1.getNome())){
                    cliente = cliente1;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ConversorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return  cliente;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value != null){
            return ((Cliente)value).getNome();
        } else{
            return null;
        }
    }
    
}

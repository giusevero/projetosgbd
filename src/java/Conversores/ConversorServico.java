/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conversores;

import DAOs.ServicosDAO;
import Models.Servicos;
import Views.IndexBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author giuse
 */

@FacesConverter("Servico")
public class ConversorServico implements Converter{

    ServicosDAO servicosDAO;
    Servicos s = new Servicos();
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        servicosDAO = new ServicosDAO();
        
        try {
            List<Servicos> serv = servicosDAO.SelecionaServicos();
            
            for (Servicos servicos : serv) {
                if(value.equals(servicos.getDescricao())){
                    s = servicos;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ConversorServico.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return  s;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value != null){
            return ((Servicos)value).getDescricao();
        } else{
            return null;
        }
    }
    
}

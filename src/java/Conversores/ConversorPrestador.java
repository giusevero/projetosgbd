/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conversores;

import DAOs.PrestadorDAO;
import Models.Prestador;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author giuse
 */

@FacesConverter("Prestador")
public class ConversorPrestador implements Converter{

    PrestadorDAO prestadorDAO;
    Prestador retorno = new Prestador();
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        prestadorDAO = new PrestadorDAO();
        
        try {
            List<Prestador> prest = prestadorDAO.ListaPrestador();
            
            for (Prestador prestador : prest) {
                if(value.equals(prestador.getNome_artistico())){
                    retorno = prestador;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ConversorPrestador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return  retorno;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value != null){
            return ((Prestador)value).getNome_artistico();
        } else {
            return null;
        }
    }
    
}

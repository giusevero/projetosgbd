/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.registry.ConnectionFactory;

/**
 *
 * @author Giugliano
 */
public class Conexao {
    /**
     * Conexao com o banco de dados local,
 cada um deve colocar os dados do usuário no qual 
 será feita a conexão com o banco
     */
    
    //URL do banco ex.: jdbc:mysql://127.0.0.1:3306/projetosgbd?zeroDateTimeBehavior=convertToNull
    private static String URL = "jdbc:mysql://127.0.0.1:3306/projetosgbd?zeroDateTimeBehavior=convertToNull";
    //Usuário para a conexão;
    private static String User = "Giugliano";
    //Senha do usuário do banco;
    private static String Password = "giovanna10";
 
    private static String Driver = "com.mysql.jdbc.Driver";
    Connection connection;

    /**
     * Método para a conexão com o banco de dados
     * @return Banco que será conectado
     */
    public Connection Conexao() {
        try {
            Class.forName(Driver);
            this.connection = DriverManager.getConnection(URL, User, Password);
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return this.connection;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.math.BigInteger;

/**
 *
 * @author giusevero
 */
public class Strings {
    
    
    public static int formataTelefone(String fone){
        int retorno = 0;
        String tel;
        
        tel = fone.replaceAll("-", "").replaceAll("\\(", "").replaceAll("\\)", "");
        tel = tel.replaceAll(" ", "");
        BigInteger integer = new BigInteger(tel);
        retorno = integer.intValue();
        //retorno = Integer.parseInt(tel);
        
        return retorno;
    }
}
